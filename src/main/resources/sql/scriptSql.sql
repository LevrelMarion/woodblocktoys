CREATE TABLE users
(
    id text primary key,
    nom text not null,
    prenom text not null,
    adresse_postale Text,
    e_mail text,
    mot_de_passe Text not null,
    num_tel Text
);

INSERT INTO users ( id, nom, prenom, adresse_postale, e_mail, mot_de_passe, num_tel)
VALUES ('1','levrel', 'marion', 'rennes', 'marion@marion.com', 'password', '000000000')


CREATE TABLE blocs
(
    id text primary key not null,
    hauteur text not null,
    base text not null,
    couleur couleur not null,
    finition Text not null,
    type_bois Text not null,
);

INSERT INTO blocs (id, hauteur, base, couleur, finition, type_bois)
VALUES ('1','Quatre', 'Carree', 'NOIR', 'Satinee', 'Pin');

CREATE TYPE couleur AS ENUM ('BLEU','BLANC','ROUGE','JAUNE','ORANGE','VERT','NOIR');

CREATE TABLE barils
(
    id text primary key not null,
    quantity_bloc int not null
);