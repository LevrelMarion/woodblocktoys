package com.woodblocktoys.WoodBlockToysEval.controllers.representation.baril;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.woodblocktoys.WoodBlockToysEval.domain.Bloc;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewBarilRepresentation {

    private int quantityBloc;
    private String idCustomer;
    private List<Bloc> idBloc;

    public int getQuantityBloc() {
        return quantityBloc;
    }

    public void setQuantityBloc(int quantityBloc) {
        this.quantityBloc = quantityBloc;
    }

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public List<Bloc> getIdBloc() {
        return idBloc;
    }

    public void setIdBloc(List<Bloc> idBloc) {
        this.idBloc = idBloc;
    }
}
