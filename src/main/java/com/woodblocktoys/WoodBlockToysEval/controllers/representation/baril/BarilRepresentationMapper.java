package com.woodblocktoys.WoodBlockToysEval.controllers.representation.baril;

import com.woodblocktoys.WoodBlockToysEval.controllers.representation.bloc.BlocRepresentation;
import com.woodblocktoys.WoodBlockToysEval.domain.Baril;
import com.woodblocktoys.WoodBlockToysEval.domain.Bloc;
import org.springframework.stereotype.Component;

@Component
public class BarilRepresentationMapper {
    public BarilRepresentation mapToDisplayableBaril(Baril baril) {
        BarilRepresentation rsltBaril = new BarilRepresentation();
        rsltBaril.setId(baril.getId());
        rsltBaril.setQuantityBloc(baril.getQuantityBloc());
        rsltBaril.setIdCustomer(baril.getIdCustomer());
        rsltBaril.setIdBloc(baril.getIdBloc());
        return rsltBaril;
    }
}
