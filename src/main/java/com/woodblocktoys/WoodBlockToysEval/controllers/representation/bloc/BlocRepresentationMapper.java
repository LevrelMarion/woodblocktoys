package com.woodblocktoys.WoodBlockToysEval.controllers.representation.bloc;

import com.woodblocktoys.WoodBlockToysEval.domain.Bloc;
import org.springframework.stereotype.Component;

@Component
public class BlocRepresentationMapper {
    public BlocRepresentation mapToDisplayableBloc(Bloc bloc) {
        BlocRepresentation rsltBloc = new BlocRepresentation();
        rsltBloc.setId(bloc.getId());
        rsltBloc.setHauteur(bloc.getHauteur());
        rsltBloc.setBase(bloc.getBase());
        rsltBloc.setCouleur(bloc.getCouleur());
        rsltBloc.setFinition(bloc.getFinition());
        rsltBloc.setTypeBois(bloc.getTypeBois());
        return rsltBloc;
    }
}
