package com.woodblocktoys.WoodBlockToysEval.controllers.representation.baril;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.woodblocktoys.WoodBlockToysEval.domain.Baril;
import com.woodblocktoys.WoodBlockToysEval.domain.Bloc;
import com.woodblocktoys.WoodBlockToysEval.domain.caracteristiques.*;

import javax.persistence.Transient;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BarilRepresentation {
    private String id;
    private int quantityBloc;
    private String idCustomer;
    private List<Bloc> idBloc;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getQuantityBloc() {
        return quantityBloc;
    }

    public void setQuantityBloc(int quantityBloc) {
        this.quantityBloc = quantityBloc;
    }

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public List<Bloc> getIdBloc() {
        return idBloc;
    }

    public void setIdBloc(List<Bloc> idBloc) {
        this.idBloc = idBloc;
    }
}
