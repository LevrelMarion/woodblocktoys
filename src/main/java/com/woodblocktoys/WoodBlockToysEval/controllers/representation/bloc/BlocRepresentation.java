package com.woodblocktoys.WoodBlockToysEval.controllers.representation.bloc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.woodblocktoys.WoodBlockToysEval.domain.caracteristiques.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BlocRepresentation {
    private String id;
    private Hauteur hauteur;
    private Bases base;
    private Couleur couleur;
    private Finition finition;
    private Bois typeBois;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Hauteur getHauteur() {
        return hauteur;
    }

    public void setHauteur(Hauteur hauteur) {
        this.hauteur = hauteur;
    }

    public Bases getBase() {
        return base;
    }

    public void setBase(Bases base) {
        this.base = base;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }

    public Finition getFinition() {
        return finition;
    }

    public void setFinition(Finition finition) {
        this.finition = finition;
    }

    public Bois getTypeBois() {
        return typeBois;
    }

    public void setTypeBois(Bois typeBois) {
        this.typeBois = typeBois;
    }


}
