package com.woodblocktoys.WoodBlockToysEval.controllers.representation.user;

import com.woodblocktoys.WoodBlockToysEval.domain.User;
import org.springframework.stereotype.Component;

@Component
public class UserRepresentationMapper {

    public UserRepresentation mapToDisplayableUtilisateur(User user) {
        UserRepresentation resultat = new UserRepresentation();
        resultat.setId(user.getId());
        resultat.setNom(user.getNom());
        resultat.setPrenom(user.getPrenom());
        resultat.setAdressePostale(user.getAdressePostale());
        resultat.seteMail(user.geteMail());
        resultat.setNumTel(user.getNumTel());
        resultat.setMotDePasse(user.getMotDePasse());
        return resultat;
    }
}
