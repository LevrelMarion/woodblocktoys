package com.woodblocktoys.WoodBlockToysEval.controllers;

import com.woodblocktoys.WoodBlockToysEval.controllers.representation.baril.BarilRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.baril.BarilRepresentationMapper;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.baril.NewBarilRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.bloc.BlocRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.bloc.BlocRepresentationMapper;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.bloc.NewBlocRepresentation;
import com.woodblocktoys.WoodBlockToysEval.domain.Baril;
import com.woodblocktoys.WoodBlockToysEval.domain.Bloc;
import com.woodblocktoys.WoodBlockToysEval.service.BarilService;
import com.woodblocktoys.WoodBlockToysEval.service.BlocService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/barils")
public class BarilController {

    private BarilService barilService;
    private BarilRepresentationMapper barilRepresentationMapper;

    @Autowired
    public BarilController(BarilService barilService, BarilRepresentationMapper barilRepresentationMapper) {
        this.barilService = barilService;
        this.barilRepresentationMapper = barilRepresentationMapper;
    }

    @GetMapping
    List<BarilRepresentation> getAllBarils() {
        return this.barilService.getAllBarils().stream()
                .map(this.barilRepresentationMapper::mapToDisplayableBaril)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<BarilRepresentation> getOneBaril(@PathVariable("id") String id) {
        Optional<Baril> baril = this.barilService.getOneBaril(id);
        return baril
                .map(this.barilRepresentationMapper::mapToDisplayableBaril)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BarilRepresentation> deleteOneBaril(@PathVariable("id") String id) {
        this.barilService.deleteOneBaril(id);
        return ResponseEntity.noContent().build();
    }

   @PostMapping
    public BarilRepresentation createBaril(@RequestBody NewBarilRepresentation body) {
        return barilRepresentationMapper.mapToDisplayableBaril(this.barilService.createOneBaril(body));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BarilRepresentation> modifyBaril(@PathVariable ("id") String id ,
                                                               @RequestBody BarilRepresentation body) {
        this.barilService.modifyOneBaril(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
