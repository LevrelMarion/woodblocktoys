package com.woodblocktoys.WoodBlockToysEval.controllers;

import com.woodblocktoys.WoodBlockToysEval.controllers.representation.user.NewUserRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.user.UserRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.user.UserRepresentationMapper;
import com.woodblocktoys.WoodBlockToysEval.domain.User;
import com.woodblocktoys.WoodBlockToysEval.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController {

    private UsersService usersService;
    private UserRepresentationMapper userRepresentationMapper;

    @Autowired
    public UserController(UsersService usersService, UserRepresentationMapper userRepresentationMapper) {
        this.usersService = usersService;
        this.userRepresentationMapper = userRepresentationMapper;
    }

    @GetMapping
    List<UserRepresentation> getAllUsers() {
        return this.usersService.getAllUsers().stream()
                .map(this.userRepresentationMapper::mapToDisplayableUtilisateur)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<UserRepresentation> getOneUser(@PathVariable("id") String id) {
        Optional<User> user = this.usersService.getOneUser(id);
        return user
                .map(this.userRepresentationMapper::mapToDisplayableUtilisateur)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<UserRepresentation> deleteOneUser(@PathVariable("id") String id) {
        this.usersService.deleteOneUser(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping
    public UserRepresentation createUser(@RequestBody NewUserRepresentation body) {
        return userRepresentationMapper.mapToDisplayableUtilisateur(this.usersService.createOneUser(body));
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserRepresentation> modifyUser(@PathVariable ("id") String id ,
                                                               @RequestBody UserRepresentation body) {
        this.usersService.modifyOneUser(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
