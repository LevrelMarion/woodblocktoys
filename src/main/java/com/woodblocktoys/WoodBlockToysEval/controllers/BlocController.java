package com.woodblocktoys.WoodBlockToysEval.controllers;

import com.woodblocktoys.WoodBlockToysEval.controllers.representation.bloc.BlocRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.bloc.BlocRepresentationMapper;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.bloc.NewBlocRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.user.NewUserRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.user.UserRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.user.UserRepresentationMapper;
import com.woodblocktoys.WoodBlockToysEval.domain.Bloc;
import com.woodblocktoys.WoodBlockToysEval.domain.User;
import com.woodblocktoys.WoodBlockToysEval.service.BlocService;
import com.woodblocktoys.WoodBlockToysEval.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/blocs")
public class BlocController {

    private BlocService blocService;
    private BlocRepresentationMapper blocRepresentationMapper;

    @Autowired
    public BlocController(BlocService blocService, BlocRepresentationMapper blocRepresentationMapper) {
        this.blocService = blocService;
        this.blocRepresentationMapper = blocRepresentationMapper;
    }

    @GetMapping
    List<BlocRepresentation> getAllBlocs() {
        return this.blocService.getAllBlocs().stream()
                .map(this.blocRepresentationMapper::mapToDisplayableBloc)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<BlocRepresentation> getOneBloc(@PathVariable("id") String id) {
        Optional<Bloc> bloc = this.blocService.getOneBloc(id);
        return bloc
                .map(this.blocRepresentationMapper::mapToDisplayableBloc)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BlocRepresentation> deleteOneBloc(@PathVariable("id") String id) {
        this.blocService.deleteOneBloc(id);
        return ResponseEntity.noContent().build();
    }

   @PostMapping
    public BlocRepresentation createBloc(@RequestBody NewBlocRepresentation body) {
        return blocRepresentationMapper.mapToDisplayableBloc(this.blocService.createOneBloc(body));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BlocRepresentation> modifyBloc(@PathVariable ("id") String id ,
                                                               @RequestBody BlocRepresentation body) {
        this.blocService.modifyOneBloc(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
