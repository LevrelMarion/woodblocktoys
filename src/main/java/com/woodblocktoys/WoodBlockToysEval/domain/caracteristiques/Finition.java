package com.woodblocktoys.WoodBlockToysEval.domain.caracteristiques;

public enum Finition {
    MATE,
    BRILLANTE,
    SATINEE,
    BRUTE,
}
