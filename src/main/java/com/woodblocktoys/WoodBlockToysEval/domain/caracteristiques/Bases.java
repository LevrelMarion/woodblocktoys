package com.woodblocktoys.WoodBlockToysEval.domain.caracteristiques;

public enum Bases {
    CARREE,
    TRIANGULAIRE,
    HEXAGONALE,
    RECTANGULAIRE,
    CYLINDRIQUE,
    }
