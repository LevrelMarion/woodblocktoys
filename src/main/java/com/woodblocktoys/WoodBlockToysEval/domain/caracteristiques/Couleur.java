package com.woodblocktoys.WoodBlockToysEval.domain.caracteristiques;

public enum Couleur {
    BLEU,
    BLANC,
    ROUGE,
    JAUNE,
    ORANGE,
    VERT,
    NOIR,
    AUCUNE;
}
