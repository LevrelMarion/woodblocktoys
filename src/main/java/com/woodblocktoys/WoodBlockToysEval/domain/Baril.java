package com.woodblocktoys.WoodBlockToysEval.domain;

import com.woodblocktoys.WoodBlockToysEval.domain.caracteristiques.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "barils")
public class Baril {

    @Id
    private String id;
    private int quantityBloc;
    private String idCustomer;
    // name : le nom de la colonne dans cette classe/table et referenced le nom dans la table à laquelle cela fait référence
    @OneToMany()
    @JoinColumn(name="baril_id")
    private List<Bloc> idBloc;

    public Baril(String id, int quantityBloc, String idCustomer, List<Bloc> idBloc) {
        this.id = id;
        this.quantityBloc = quantityBloc;
        this.idCustomer = idCustomer;
        this.idBloc = idBloc;
    }

    protected Baril() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getQuantityBloc() {
        return quantityBloc;
    }

    public void setQuantityBloc(int quantityBloc) {
        this.quantityBloc = quantityBloc;
    }

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public List<Bloc> getIdBloc() {
        return idBloc;
    }

    public void setIdBloc(List<Bloc> idBloc) {
        this.idBloc = idBloc;
    }
}