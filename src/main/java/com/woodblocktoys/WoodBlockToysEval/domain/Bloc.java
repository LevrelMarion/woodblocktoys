package com.woodblocktoys.WoodBlockToysEval.domain;

import com.woodblocktoys.WoodBlockToysEval.domain.caracteristiques.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "blocs")
public class Bloc {

    @Id
    private String id;
    @Enumerated(value = EnumType.STRING)
    private Hauteur hauteur;
    @Enumerated(value = EnumType.STRING)
    private Bases base;
    @Enumerated(value = EnumType.STRING)
    private Couleur couleur;
    @Enumerated(value = EnumType.STRING)
    private Finition finition;
    @Enumerated(value = EnumType.STRING)
    private Bois typeBois;

    public Bloc(String id, Hauteur hauteur, Bases base, Couleur couleur, Finition finition, Bois typeBois) {
        this.id = id;
        this.hauteur = hauteur;
        this.base = base;
        this.couleur = couleur;
        this.finition = finition;
        this.typeBois = typeBois;
    }

    protected Bloc() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Hauteur getHauteur() {
        return hauteur;
    }

    public void setHauteur(Hauteur hauteur) {
        this.hauteur = hauteur;
    }

    public Bases getBase() {
        return base;
    }

    public void setBase(Bases base) {
        this.base = base;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }

    public Finition getFinition() {
        return finition;
    }

    public void setFinition(Finition finition) {
        this.finition = finition;
    }

    public Bois getTypeBois() {
        return typeBois;
    }

    public void setTypeBois(Bois typeBois) {
        this.typeBois = typeBois;
    }

}
