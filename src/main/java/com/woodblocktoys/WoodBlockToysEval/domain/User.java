package com.woodblocktoys.WoodBlockToysEval.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {
    @Id
    private String id;
    private String nom;
    private String prenom;
    private String adressePostale;
    private String eMail;
    private String motDePasse;
    private String numTel;

    public User(String id, String nom, String prenom, String adressePostale, String eMail, String motDePasse, String numTel) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.adressePostale = adressePostale;
        this.eMail = eMail;
        this.motDePasse = motDePasse;
        this.numTel = numTel;
    }

    protected User() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdressePostale() {
        return adressePostale;
    }

    public void setAdressePostale(String adressePostale) {
        this.adressePostale = adressePostale;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getNumTel() {
        return numTel;
    }

    public void setNumTel(String numTel) {
        this.numTel = numTel;
    }
}
