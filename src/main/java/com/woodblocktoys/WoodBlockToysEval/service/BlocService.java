package com.woodblocktoys.WoodBlockToysEval.service;

import com.woodblocktoys.WoodBlockToysEval.controllers.representation.bloc.BlocRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.bloc.NewBlocRepresentation;
import com.woodblocktoys.WoodBlockToysEval.domain.Baril;
import com.woodblocktoys.WoodBlockToysEval.domain.Bloc;
import com.woodblocktoys.WoodBlockToysEval.domain.caracteristiques.*;
import com.woodblocktoys.WoodBlockToysEval.repositories.BlocRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class BlocService {

    private BlocRepository blocRepository;
    private IdGenerator idGenerator;

    public BlocService(BlocRepository blocRepository, IdGenerator idGenerator) {
        this.blocRepository = blocRepository;
        this.idGenerator = idGenerator;
    }

    public List<Bloc> getAllBlocs() {
        return (List<Bloc>) blocRepository.findAll();
    }

    public Optional<Bloc> getOneBloc(String id) {
        return this.blocRepository.findById(id);
    }

    public void deleteOneBloc(String id) {
        this.blocRepository.deleteById(id);
    }

    public Bloc createOneBloc(NewBlocRepresentation body) {
        if (body.getTypeBois().equals(Bois.ACAJOU) || body.getTypeBois().equals(Bois.CHENE) || body.getTypeBois().equals(Bois.HETRE)) {
            Bloc b = new Bloc(this.idGenerator.generateNewId(), body.getHauteur(),
                    body.getBase(), Couleur.AUCUNE, Finition.BRUTE, body.getTypeBois());
            return this.blocRepository.save(b);
        } else if (body.getTypeBois().equals(Bois.PIN)) {
            Bloc bloc = new Bloc(this.idGenerator.generateNewId(), body.getHauteur(), body.getBase(),
                    body.getCouleur(), body.getFinition(), body.getTypeBois());
            return this.blocRepository.save(bloc);
        }
        return null;
    }

    public Bloc modifyOneBloc(String id, BlocRepresentation body) {
        Bloc blocToModify = blocRepository.findById(id).orElseThrow();
        Bloc blocInBase = new Bloc(id, body.getHauteur(), body.getBase(),
                body.getCouleur(), body.getFinition(), body.getTypeBois());
        Hauteur newHauteur = null;
        Bases newBase = null;
        Couleur newCouleur = null;
        Finition newFinition = null;
        Bois newTypeBois = null;

        if (blocInBase.getHauteur().equals(null)) {
            newHauteur = blocToModify.getHauteur();
        } else {
            newHauteur =blocInBase.getHauteur();
        }
        if (blocInBase.getBase().equals(null)) {
            newBase = blocToModify.getBase();
        } else {
            newBase = blocInBase.getBase();
        }
        if (blocInBase.getCouleur().equals(null)) {
            newCouleur = blocToModify.getCouleur();
        } else {
            newCouleur = blocInBase.getCouleur();
        }
        if (blocInBase.getFinition().equals(null)) {
            newFinition = blocToModify.getFinition();
        } else {
            newFinition = blocInBase.getFinition();
        }
        if (blocInBase.getTypeBois().equals(null)) {
            newTypeBois = blocToModify.getTypeBois();
        } else {
            newTypeBois = blocInBase.getTypeBois();
        }

        Bloc newBloc = new Bloc(id, newHauteur, newBase, newCouleur, newFinition, newTypeBois);
        return this.blocRepository.save(newBloc);
    }
}