package com.woodblocktoys.WoodBlockToysEval.service;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class IdGenerator {

    private final UUID counter = null;

    public String generateNewId() {
        return this.counter.randomUUID().toString();
    }
}

