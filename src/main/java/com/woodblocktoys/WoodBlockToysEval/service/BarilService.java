package com.woodblocktoys.WoodBlockToysEval.service;

import com.woodblocktoys.WoodBlockToysEval.controllers.representation.baril.BarilRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.baril.NewBarilRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.bloc.BlocRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.bloc.NewBlocRepresentation;
import com.woodblocktoys.WoodBlockToysEval.domain.Baril;
import com.woodblocktoys.WoodBlockToysEval.domain.Bloc;
import com.woodblocktoys.WoodBlockToysEval.domain.caracteristiques.*;
import com.woodblocktoys.WoodBlockToysEval.repositories.BarilRepository;
import com.woodblocktoys.WoodBlockToysEval.repositories.BlocRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class BarilService {

    private BarilRepository barilRepository;
    private IdGenerator idGenerator;

    public BarilService(BarilRepository barilRepository, IdGenerator idGenerator) {
        this.barilRepository = barilRepository;
        this.idGenerator = idGenerator;
    }

    public List<Baril> getAllBarils() {
        return (List<Baril>) barilRepository.findAll();
    }

    public Optional<Baril> getOneBaril(String id) {
        return this.barilRepository.findById(id);
    }

    public void deleteOneBaril(String id) {
        this.barilRepository.deleteById(id);
    }

    public Baril createOneBaril(NewBarilRepresentation body) {
             Baril baril = new Baril(this.idGenerator.generateNewId(), body.getQuantityBloc(), body.getIdCustomer(), body.getIdBloc());
            return this.barilRepository.save(baril);
        }

    public Baril modifyOneBaril(String id, BarilRepresentation body) {
        Baril barilToModify = barilRepository.findById(id).orElseThrow();
        Baril barilInBase = new Baril(id, body.getQuantityBloc(), body.getIdCustomer(), body.getIdBloc());
        int newQuantityBloc = 0;
        String newIdCustomer = "";
        List<Bloc> newIdBloc = null;

        if (barilInBase.getQuantityBloc() == 0) {
            newQuantityBloc = barilToModify.getQuantityBloc();
        } else {
            newQuantityBloc =barilInBase.getQuantityBloc();
        }
        if (barilInBase.getIdCustomer().equals("")) {
            newIdCustomer = barilToModify.getIdCustomer();
        } else {
            newIdCustomer = barilInBase.getIdCustomer();
        }
        if (barilInBase.getIdBloc().equals(null)) {
            newIdBloc = barilToModify.getIdBloc();
        } else {
            newIdBloc = barilInBase.getIdBloc();
        }


        Baril newBaril = new Baril(id, newQuantityBloc, newIdCustomer, newIdBloc);
        return this.barilRepository.save(newBaril);
    }
}