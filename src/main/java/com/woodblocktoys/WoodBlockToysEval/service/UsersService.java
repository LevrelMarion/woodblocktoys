package com.woodblocktoys.WoodBlockToysEval.service;

import com.woodblocktoys.WoodBlockToysEval.controllers.representation.user.NewUserRepresentation;
import com.woodblocktoys.WoodBlockToysEval.controllers.representation.user.UserRepresentation;
import com.woodblocktoys.WoodBlockToysEval.domain.User;
import com.woodblocktoys.WoodBlockToysEval.repositories.UserRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UsersService {

    private UserRepository userRepository;
    private IdGenerator idGenerator;

    public UsersService(UserRepository userRepository, IdGenerator idGenerator) {
        this.userRepository = userRepository;
        this.idGenerator = idGenerator;
    }

    public List<User> getAllUsers() {
        return (List<User>) this.userRepository.findAll();
    }

    public Optional<User> getOneUser(String id) {
        return this.userRepository.findById(id);
    }

    public void deleteOneUser(String id) {
        this.userRepository.deleteById(id);
    }

    public User createOneUser(NewUserRepresentation body) {
        User user = new User(this.idGenerator.generateNewId(), body.getNom(), body.getPrenom(),
                body.getAdressePostale(), body.geteMail(), body.getMotDePasse(), body.getNumTel());
        return this.userRepository.save(user);
    }

    public User modifyOneUser(String id, UserRepresentation body) {
        User userToModify = userRepository.findById(id).orElseThrow();
        User userInBase = new User(id, body.getNom(), body.getPrenom(),
                body.getAdressePostale(), body.geteMail(), body.getMotDePasse(), body.getNumTel());
        String newNom = "";
        String newPrenom = "";
        String newAdressePostale = "";
        String newEMail = "";
        String newMotDePasse = "";
        String newNumTel = "";

        if (userInBase.getNom().equals("")) {
            newNom = userToModify.getNom();
        } else {
            newNom = userInBase.getNom();
        }
        if (userInBase.getPrenom().equals("")) {
            newPrenom = userToModify.getPrenom();
        } else {
            newPrenom = userInBase.getPrenom();
        }
        if (userInBase.getAdressePostale().isEmpty()) {
            newAdressePostale = userToModify.getAdressePostale();
        } else {
            newAdressePostale = userInBase.getAdressePostale();
        }
        if (userInBase.geteMail().equals("")) {
            newEMail = userToModify.geteMail();
        } else {
            newEMail = userInBase.geteMail();
        }
        if (userInBase.getMotDePasse().equals("")) {
            newMotDePasse = userToModify.getMotDePasse();
        } else {
            newMotDePasse = userInBase.getMotDePasse();
        }
        if (userInBase.getNumTel().equals("")) {
            newNumTel = userToModify.getNumTel();
        } else {
            newNumTel = userInBase.getNumTel();
        }

        User newUser = new User(id, newNom, newPrenom, newAdressePostale, newEMail, newMotDePasse, newNumTel);
        return this.userRepository.save(newUser);
    }
}