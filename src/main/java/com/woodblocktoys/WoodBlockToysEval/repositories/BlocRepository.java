package com.woodblocktoys.WoodBlockToysEval.repositories;

import com.woodblocktoys.WoodBlockToysEval.domain.Bloc;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlocRepository extends CrudRepository<Bloc, String> {

}
