package com.woodblocktoys.WoodBlockToysEval.repositories;

import com.woodblocktoys.WoodBlockToysEval.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
}
