package com.woodblocktoys.WoodBlockToysEval.repositories;

import com.woodblocktoys.WoodBlockToysEval.domain.Baril;
import com.woodblocktoys.WoodBlockToysEval.domain.Bloc;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BarilRepository extends CrudRepository<Baril, String> {

}
