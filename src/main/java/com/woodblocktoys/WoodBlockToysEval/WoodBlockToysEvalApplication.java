package com.woodblocktoys.WoodBlockToysEval;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WoodBlockToysEvalApplication {

	public static void main(String[] args) {
		SpringApplication.run(WoodBlockToysEvalApplication.class, args);
	}

}
